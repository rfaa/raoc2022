use std::collections::VecDeque;

use raoc2022::input;

struct Instruction {
    m: usize,
    f: usize,
    t: usize,
}

fn main() {
    let input = input("day05.tx");

    part1(&input);
    part2(&input);
}

fn part1(input: &[String]) {
    let (mut stacks, inststructions) = parse(input);

    for i in inststructions {
        for _ in (0..i.m).rev() {
            let sf = stacks.get_mut(i.f - 1).unwrap();
            let c = sf.pop_back().unwrap();
            let st = stacks.get_mut(i.t - 1).unwrap();
            st.push_back(c);
        }
    }

    for mut s in stacks {
        print!("{}", s.pop_back().unwrap());
    }
    println!();
}

fn part2(input: &[String]) {
    let (mut stacks, instructions) = parse(input);
    let mut tmp = VecDeque::new();

    for i in instructions {
        for _ in (0..i.m).rev() {
            let sf = stacks.get_mut(i.f - 1).unwrap();
            let c = sf.pop_back().unwrap();
            tmp.push_front(c);
        }

        let st = stacks.get_mut(i.t - 1).unwrap();
        while let Some(c) = tmp.pop_front() {
            st.push_back(c);
        }
    }

    for mut s in stacks {
        print!("{}", s.pop_back().unwrap());
    }
    println!();
}

fn parse(input: &[String]) -> (Vec<VecDeque<char>>, Vec<Instruction>) {
    let mut stacks: Vec<VecDeque<char>> = Vec::new();
    let mut instructions = Vec::new();
    let mut found_instructions = false;

    for row in input {
        // empty row means the instructions are incoming
        if row.is_empty() {
            found_instructions = true;
            continue;
        }

        match found_instructions {
            true => {
                let mi = row.find("move ").unwrap();
                let fi = row.find("from ").unwrap();
                let ti = row.find("to ").unwrap();

                let m = row[mi + 5..fi - 1].parse::<usize>().unwrap();
                let f = row[fi + 5..ti - 1].parse::<usize>().unwrap();
                let t = row[ti + 3..].parse::<usize>().unwrap();

                instructions.push(Instruction { m, f, t });
            }
            false => {
                let b = row.as_bytes();

                // the row with the stack numbers start with 1 at index 1, simply skip
                if b[1] as char == '1' {
                    continue;
                }

                let mut i = 0;
                let mut idx = 0;
                while i < b.len() {
                    i += 1;

                    let s = match stacks.get_mut(idx) {
                        Some(v) => v,
                        None => {
                            stacks.push(VecDeque::new());
                            stacks.get_mut(idx).unwrap()
                        }
                    };

                    let a = b[i] as char;
                    if a != ' ' {
                        s.push_front(a);
                    }

                    i += 3;
                    idx += 1;
                }
            }
        }
    }

    (stacks, instructions)
}
