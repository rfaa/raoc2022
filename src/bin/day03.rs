use raoc2022::input;
use std::collections::HashSet;

fn main() {
    let input = input("day03.txt");

    part1(&input);
    part2(&input);
}

fn part1(input: &[String]) {
    let mut sum = 0;
    for row in input {
        let c = row.chars().count();
        let p1 = &row[0..c / 2];
        let p2 = &row[c / 2..];
        let mut m = HashSet::new();
        // a = 97
        // a - 96 = 1
        // z = 122
        // z - 96 = 26
        // A = 65
        // A - 38 = 27
        // Z = 90
        // Z - 38 = 52
        for ch in p1.as_bytes() {
            if p2.contains(*ch as char) {
                let i = *ch as i32;
                let i = if i >= 97 { i - 96 } else { i - 38 };
                m.insert(i);
            }
        }

        for i in m {
            sum += i;
        }
    }

    println!("sum: {}", sum);
}

fn part2(input: &[String]) {
    let mut sum = 0;
    let mut i = 0;
    while i < input.len() - 2 {
        for ch in input[i].as_bytes() {
            if !&input[i + 1].contains(*ch as char) {
                continue;
            }

            if !&input[i + 2].contains(*ch as char) {
                continue;
            }

            i += 2;

            let s = *ch as i32;
            sum += if s >= 97 { s - 96 } else { s - 38 };

            break;
        }

        i += 1;
    }

    println!("sum: {}", sum);
}
