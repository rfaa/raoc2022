use raoc2022::input;

fn main() {
    let input = input("day08.txt");

    part1(&input);
    part2(&input);
}

fn part1(input: &[String]) {
    let trees = parse(input);
    let mut vis = trees.len() * 2 + trees[0].len() * 2 - 4;

    // skip outer parts, they are always visible
    for y in 1..trees.len() - 1 {
        for x in 1..trees[y].len() - 1 {
            let tree = trees[y][x];

            // up
            if (0..y).all(|yy| trees[yy][x] < tree) {
                vis += 1;
                continue;
            }

            // down
            if (y + 1..trees.len()).all(|yy| trees[yy][x] < tree) {
                vis += 1;
                continue;
            }

            // left
            if (0..x).all(|xx| trees[y][xx] < tree) {
                vis += 1;
                continue;
            }

            // right
            if (x + 1..trees[y].len()).all(|xx| trees[y][xx] < tree) {
                vis += 1;
                continue;
            }
        }
    }

    println!("visible: {}", vis);
}

fn part2(input: &[String]) {
    let trees = parse(input);
    let mut score = 0;

    // the outer parts = multiplication with 0 -> 0, skip
    for y in 1..trees.len() - 1 {
        for x in 1..trees[y].len() - 1 {
            let tree = trees[y][x];

            // the blocking tree still counts as one

            let up = (1..y).rev().take_while(|yy| trees[*yy][x] < tree).count() + 1;

            let down = (y + 1..trees.len() - 1)
                .take_while(|yy| trees[*yy][x] < tree)
                .count()
                + 1;

            let left = (1..x).rev().take_while(|xx| trees[y][*xx] < tree).count() + 1;

            let right = (x + 1..trees[y].len() - 1)
                .take_while(|xx| trees[y][*xx] < tree)
                .count()
                + 1;

            let s = up * down * left * right;

            if s > score {
                score = s;
            }
        }
    }

    println!("score: {}", score);
}

fn parse(input: &[String]) -> Vec<Vec<usize>> {
    let mut trees = Vec::new();

    for row in input {
        let mut vec = Vec::new();

        for c in row.chars() {
            // ascii hack
            vec.push(c as usize - 48);
        }

        trees.push(vec);
    }

    trees
}
