use raoc2022::input;

fn main() {
    let input = input("day01.txt");

    part1(&input);
    part2(&input);
}

fn part1(input: &[String]) {
    let mut max = 0;
    let mut tmp = 0;

    for row in input {
        if row.is_empty() {
            if tmp > max {
                max = tmp;
            }

            tmp = 0;

            continue;
        }

        tmp += row.parse::<i32>().unwrap();
    }

    println!("max: {}", max);
}

fn part2(input: &[String]) {
    let mut max1 = 0;
    let mut max2 = 0;
    let mut max3 = 0;
    let mut tmp = 0;

    for row in input {
        if row.is_empty() {
            if tmp > max1 {
                max3 = max2;
                max2 = max1;
                max1 = tmp;
            } else if tmp > max2 {
                max3 = max2;
                max2 = tmp;
            } else if tmp > max3 {
                max3 = tmp;
            }

            tmp = 0;

            continue;
        }

        tmp += row.parse::<i32>().unwrap();
    }

    println!("total: {}", max1 + max2 + max3);
}
