use raoc2022::input;

fn main() {
    let input = input("day0.txt");

    part1(&input);
    part2(&input);
}

fn part1(input: &[String]) {}

fn part2(input: &[String]) {}
