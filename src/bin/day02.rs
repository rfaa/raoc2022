use raoc2022::input;

fn main() {
    let input = input("day02.txt");

    part1(&input);
    part2(&input);
}

fn part1(input: &[String]) {
    let mut score = 0;

    for row in input {
        // loss: 0
        // draw: 3
        // win: 6
        // rock: 1
        // paper: 2
        // scissors: 3
        // A/X - rock
        // B/Y - paper
        // C/Z - scissors
        let answers: Vec<&str> = row.split(' ').collect();
        let u = answers[0];
        let i = answers[1];

        match (u, i) {
            ("A", "Z") | ("B", "X") | ("C", "Y") => {
                // lose
                score += 0;
            }
            ("A", "X") | ("B", "Y") | ("C", "Z") => {
                // draw
                score += 3;
            }
            ("A", "Y") | ("B", "Z") | ("C", "X") => {
                // win
                score += 6;
            }
            _ => (),
        }

        match i {
            "X" => score += 1,
            "Y" => score += 2,
            "Z" => score += 3,
            _ => (),
        }
    }

    println!("score: {}", score);
}

fn part2(input: &[String]) {
    // x: lose
    // y: draw
    // z: win
    let mut score = 0;

    // loss: 0
    // draw: 3
    // win: 6
    // rock: 1
    // paper: 2
    // scissors: 3
    // A/X - rock
    // B/Y - paper
    // C/Z - scissors
    for row in input {
        let answers: Vec<&str> = row.split(' ').collect();
        let u = answers[0];
        let i = answers[1];

        match u {
            "A" => {
                match i {
                    "X" => {
                        // lose
                        score += 3; // s
                        score += 0; // l
                    }
                    "Y" => {
                        // draw
                        score += 1; // r
                        score += 3; // d
                    }
                    "Z" => {
                        // win
                        score += 2; // p
                        score += 6; // w
                    }
                    _ => (),
                }
            }
            "B" => {
                match i {
                    "X" => {
                        // lose
                        score += 1; // s
                        score += 0; // l
                    }
                    "Y" => {
                        // draw
                        score += 2; // p
                        score += 3; // d
                    }
                    "Z" => {
                        // win
                        score += 3; // s
                        score += 6; // w
                    }
                    _ => (),
                }
            }
            "C" => {
                match i {
                    "X" => {
                        // lose
                        score += 2; // p
                        score += 0; // l
                    }
                    "Y" => {
                        // draw
                        score += 3; // s
                        score += 3; // d
                    }
                    "Z" => {
                        // win
                        score += 1; // r
                        score += 6;
                    }
                    _ => (),
                }
            }
            _ => (),
        }
    }

    println!("score: {}", score);
}
