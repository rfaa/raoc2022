use raoc2022::input;

fn main() {
    let input = input("day06.txt");

    part1(&input);
    part2(&input);
}

fn part1(input: &[String]) {
    // there should only be one row
    let input = input[0].as_bytes();

    for i in 0..input.len() - 4 {
        if !check(input[i], &input[i + 1..i + 4]) {
            continue;
        }

        println!(
            "found {} - {}{}{}{}",
            i + 4,
            input[i] as char,
            input[i + 1] as char,
            input[i + 2] as char,
            input[i + 3] as char
        );
        break;
    }
}

fn part2(input: &[String]) {
    // there should only be one row
    let input = input[0].as_bytes();

    for i in 0..input.len() - 14 {
        if !check(input[i], &input[i + 1..i + 14]) {
            continue;
        }

        println!(
            "found {} - {}{}{}{}",
            i + 14,
            input[i] as char,
            input[i + 1] as char,
            input[i + 2] as char,
            input[i + 3] as char
        );
        break;
    }
}

fn check(c: u8, s: &[u8]) -> bool {
    if s.len() == 1 {
        return c != s[0];
    }

    for i in 0..s.len() {
        if c == s[i] {
            return false;
        }
    }

    check(s[0], &s[1..])
}
