use raoc2022::input;

struct FileSystem {
    nodes: Vec<Node>,
    files: Vec<FileType>,
}

struct Node {
    parent: Option<usize>,
    children: Vec<usize>,
    fidx: usize,
}

enum FileType {
    File(usize, u32, String),
    Dir(usize, String),
}

fn main() {
    let input = input("day07.txt");

    part1(&input);
    part2(&input);
}

fn part1(input: &[String]) {
    let filesystem = parse(input);
    let mut sz = 0;

    for node in &filesystem.nodes {
        let s = size(node.fidx, &filesystem);
        let n = path(&filesystem.files[node.fidx], &filesystem);

        println!("{} {}", n, s);

        if s <= 100000 {
            sz += s;
        }
    }

    println!();
    println!("size: {}", sz);
}

fn part2(input: &[String]) {
    let filesystem = parse(input);
    let tot_space = 70000000;
    let req_space = 30000000;
    let use_space = size(0, &filesystem);
    let fre_space = tot_space - use_space;

    let mut candidate = tot_space;

    for node in &filesystem.nodes {
        let sz = size(node.fidx, &filesystem);

        if fre_space + sz >= req_space && sz < candidate {
            candidate = sz;
        }
    }

    println!("size: {}", candidate);
}

fn path(file: &FileType, filesystem: &FileSystem) -> String {
    match file {
        FileType::File(nidx, _, name) => {
            let node = &filesystem.nodes[*nidx];
            path(&filesystem.files[node.fidx], filesystem) + name
        }
        FileType::Dir(nidx, name) => {
            let node = &filesystem.nodes[*nidx];
            match node.parent {
                Some(parent) => {
                    let n = &filesystem.nodes[parent];
                    path(&filesystem.files[n.fidx], filesystem) + name + "/"
                }
                None => name.to_owned(),
            }
        }
    }
}

fn size(file_idx: usize, filesystem: &FileSystem) -> u32 {
    match &filesystem.files[file_idx] {
        FileType::File(_, sz, _) => *sz,
        FileType::Dir(nidx, _) => {
            let node = &filesystem.nodes[*nidx];
            let mut sz = 0;

            for fidx in &node.children {
                sz += size(*fidx, filesystem);
            }

            sz
        }
    }
}

fn parse(input: &[String]) -> FileSystem {
    let mut filesystem = FileSystem {
        nodes: Vec::new(),
        files: Vec::new(),
    };

    // first row will always be 'cd /'
    filesystem.nodes.push(Node {
        parent: None,
        children: Vec::new(),
        fidx: 0,
    });

    filesystem.files.push(FileType::Dir(0, "/".to_owned()));

    let mut current_nidx = 0;
    let mut i = 1;

    while i < input.len() {
        // second row will always be a command, this is fine
        let cmd = input[i].get(2..4).unwrap();

        match cmd {
            "cd" => {
                let dir = input[i].get(5..).unwrap();
                let node = &filesystem.nodes[current_nidx];

                match dir {
                    ".." => {
                        current_nidx = node.parent.unwrap();
                    }
                    _ => {
                        // we will always find a new current node index
                        for fidx in &node.children {
                            if let FileType::Dir(node_idx, name) = &filesystem.files[*fidx] {
                                if name == dir {
                                    current_nidx = *node_idx;
                                    break;
                                }
                            }
                        }
                    }
                }

                i += 1;
            }
            "ls" => {
                i += 1;

                while i < input.len() {
                    let file: Vec<&str> = input[i].split(' ').collect();

                    // found a command, no more output from ls
                    if file[0].chars().nth(0) == Some('$') {
                        break;
                    }

                    let fidx = filesystem.files.len();

                    let node = filesystem.nodes.get_mut(current_nidx).unwrap();
                    node.children.push(fidx);

                    match file[0] {
                        "dir" => {
                            let nidx = filesystem.nodes.len();

                            filesystem
                                .files
                                .push(FileType::Dir(nidx, file[1].to_owned()));

                            filesystem.nodes.push(Node {
                                parent: Some(current_nidx),
                                children: Vec::new(),
                                fidx,
                            });
                        }
                        size => {
                            filesystem.files.push(FileType::File(
                                current_nidx,
                                size.parse::<u32>().unwrap(),
                                file[1].to_owned(),
                            ));
                        }
                    }

                    i += 1;
                }
            }
            _ => {
                panic!("uh oh");
            }
        }
    }

    filesystem
}
