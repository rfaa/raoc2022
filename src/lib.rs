use std::fs;

pub fn input(input: &str) -> Vec<String> {
    fs::read_to_string("src/bin/".to_owned() + input)
        .expect("input file error")
        .lines()
        .map(|x| x.parse::<String>().expect("invalid input"))
        .collect()
}
